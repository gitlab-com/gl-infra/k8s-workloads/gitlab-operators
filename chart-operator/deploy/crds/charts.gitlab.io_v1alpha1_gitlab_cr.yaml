apiVersion: charts.gitlab.io/v1alpha1
kind: Gitlab
metadata:
  name: example-gitlab
spec:
  # Default values copied from <project_dir>/helm-charts/gitlab/values.yaml
  
  certmanager:
    createCustomResource: true
    install: true
    nameOverride: cert-manager
    rbac:
      create: true
  gitlab-runner:
    install: true
    rbac:
      create: true
    runners:
      cache:
        cacheShared: true
        cacheType: s3
        s3BucketLocation: us-east-1
        s3BucketName: runner-cache
        s3CacheInsecure: false
        s3CachePath: gitlab-runner
      locked: false
  global:
    antiAffinity: soft
    appConfig:
      artifacts:
        bucket: gitlab-artifacts
        connection: {}
        enabled: true
        proxy_download: true
      backups:
        bucket: gitlab-backups
        tmpBucket: tmp
      cron_jobs: {}
      defaultCanCreateGroup: true
      defaultProjectsFeatures:
        builds: true
        issues: true
        mergeRequests: true
        snippets: true
        wiki: true
      defaultTheme: null
      enableImpersonation: null
      enableUsagePing: true
      externalDiffs:
        bucket: gitlab-mr-diffs
        connection: {}
        proxy_download: true
        when: null
      extra:
        googleAnalyticsId: null
        piwikSiteId: null
        piwikUrl: null
      gravatar:
        plainUrl: null
        sslUrl: null
      incomingEmail:
        address: ""
        enabled: false
        host: imap.gmail.com
        idleTimeout: 60
        mailbox: inbox
        password:
          key: password
          secret: ""
        port: 993
        ssl: true
        startTls: false
        user: ""
      issueClosingPattern: null
      ldap:
        preventSignin: false
        servers: {}
      lfs:
        bucket: git-lfs
        connection: {}
        enabled: true
        proxy_download: true
      maxRequestDurationSeconds: null
      omniauth:
        allowBypassTwoFactor: []
        allowSingleSignOn:
        - saml
        autoLinkLdapUser: false
        autoLinkSamlUser: false
        autoSignInWithProvider: null
        blockAutoCreatedUsers: true
        enabled: false
        externalProviders: []
        providers: []
        syncProfileAttributes:
        - email
        syncProfileFromProvider: []
      packages:
        bucket: gitlab-packages
        connection: {}
        enabled: true
        proxy_download: true
      pseudonymizer:
        bucket: gitlab-pseudo
        configMap: null
        connection: {}
      uploads:
        bucket: gitlab-uploads
        connection: {}
        enabled: true
        proxy_download: true
      usernameChangingEnabled: true
      webhookTimeout: null
    application:
      allowClusterRoles: true
      create: false
      links: []
    busybox:
      image:
        repository: busybox
        tag: latest
    certificates:
      customCAs: []
      image:
        repository: registry.gitlab.com/gitlab-org/build/cng/alpine-certificates
        tag: 20171114-r3
    deployment:
      annotations: {}
    edition: ee
    email:
      display_name: GitLab
      from: ""
      reply_to: ""
      smime:
        certName: tls.crt
        enabled: false
        keyName: tls.key
        secretName: ""
      subject_suffix: ""
    geo:
      enabled: false
      nodeName: null
      psql:
        password: {}
      role: primary
    gitaly:
      authToken: {}
      enabled: true
      external: []
      internal:
        names:
        - default
      tls:
        enabled: false
    gitlab:
      license: {}
    grafana:
      enabled: false
    hosts:
      domain: example.com
      externalIP: null
      gitlab: {}
      hostSuffix: null
      https: true
      minio: {}
      registry: {}
      ssh: null
      tls: {}
    ingress:
      annotations: {}
      configureCertmanager: true
      enabled: true
      tls: {}
    initialRootPassword: {}
    kubectl:
      image:
        pullSecrets: []
        repository: registry.gitlab.com/gitlab-org/build/cng/kubectl
        tag: 1.13.12
    minio:
      credentials: {}
      enabled: true
    operator:
      enabled: false
      rollout:
        autoPause: true
    psql:
      password: {}
    railsSecrets: {}
    redis:
      password:
        enabled: true
    registry:
      bucket: registry
      certificate: {}
      httpSecret: {}
    runner:
      registrationToken: {}
    service:
      annotations: {}
    shell:
      authToken: {}
      hostKeys: {}
    smtp:
      address: smtp.mailgun.org
      authentication: plain
      enabled: false
      openssl_verify_mode: peer
      password:
        key: password
        secret: ""
      port: 2525
      starttls_auto: false
      user_name: ""
    time_zone: UTC
    unicorn:
      workerTimeout: 60
    workhorse: {}
  grafana:
    admin:
      existingSecret: bogus
    command:
    - sh
    - -x
    - /tmp/scripts/import-secret.sh
    env:
      GF_SECURITY_ADMIN_PASSWORD: bogus
      GF_SECURITY_ADMIN_USER: bogus
    extraConfigmapMounts:
    - configMap: gitlab-grafana-import-secret
      mountPath: /tmp/scripts
      name: import-secret
      readOnly: true
    extraSecretMounts:
    - defaultMode: 400
      mountPath: /tmp/initial
      name: initial-password
      readOnly: true
      secretName: gitlab-grafana-initial-password
    grafana.ini:
      server:
        root_url: http://localhost/-/grafana/
    sidecar:
      dashboards:
        enabled: true
        label: gitlab_grafana_dashboard
      datasources:
        enabled: true
        label: gitlab_grafana_datasource
    testFramework:
      enabled: false
  nginx-ingress:
    controller:
      config:
        enable-vts-status: "true"
        hsts-include-subdomains: "false"
        server-name-hash-bucket-size: "256"
        server-tokens: "false"
        ssl-ciphers: ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4
        ssl-protocols: TLSv1.3 TLSv1.2
        use-http2: "false"
      extraArgs:
        force-namespace-isolation: ""
      metrics:
        enabled: true
        service:
          annotations:
            prometheus.io/port: "10254"
            prometheus.io/scrape: "true"
      minAvailable: 2
      publishService:
        enabled: true
      replicaCount: 3
      resources:
        requests:
          cpu: 100m
          memory: 100Mi
      scope:
        enabled: true
      service:
        externalTrafficPolicy: Local
      stats:
        enabled: true
    defaultBackend:
      minAvailable: 1
      replicaCount: 2
      resources:
        requests:
          cpu: 5m
          memory: 5Mi
    enabled: true
    rbac:
      create: true
    serviceAccount:
      create: true
    tcpExternalConfig: "true"
  postgresql:
    existingSecret: bogus
    image:
      tag: 10.9.0
    initdbScriptsConfigMap: bogus
    install: true
    metrics:
      enabled: true
    postgresqlDatabase: gitlabhq_production
    postgresqlPostgresPassword: bogus
    postgresqlUsername: gitlab
    usePasswordFile: true
  prometheus:
    alertmanager:
      enabled: false
    alertmanagerFiles:
      alertmanager.yml: {}
    install: true
    kubeStateMetrics:
      enabled: false
    nodeExporter:
      enabled: false
    pushgateway:
      enabled: false
    rbac:
      create: true
    server:
      retention: 15d
  redis:
    cluster:
      enabled: false
    existingSecret: gitlab-redis-secret
    existingSecretKey: redis-password
    install: true
    metrics:
      enabled: true
    usePasswordFile: true
  shared-secrets:
    enabled: true
    rbac:
      create: true
  upgradeCheck:
    enabled: true
    image: {}
    resources:
      requests:
        cpu: 50m
    securityContext:
      fsGroup: 65534
      runAsUser: 65534
    tolerations: []
  
